let defaultConfig = {
  port: process.env.PORT,
  cryptoApiUrl: 'https://api.cryptonator.com/api/ticker'
};

module.exports = { ...defaultConfig };