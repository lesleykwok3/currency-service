const Router = require("koa-router");
const router = new Router();

router.get("/currencies", require("../controller/currency-controller/get-currency-list"));

module.exports = router;
