const HTTP_STATUS = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  SERVER_ERROR: 500
};

const CURRENCY_LIST = [
  'btc-usd',
  'eth-usd',
  'ltc-usd',
  'xmr-usd',
  'xrp-usd',
  'doge-usd',
  'dash-usd',
  'maid-usd',
  'lsk-usd',
  'sjcx-usd'
]

module.exports = {
  HTTP_STATUS,
  CURRENCY_LIST
};
