const { cryptoApiUrl } = require("../config");
const api = require("../lib/api");
const logger = require("../lib/logger");

const queryCryptoCurrencyRealtimeData = async code => {
  let apiResult = await api({ method: "get", url: `${cryptoApiUrl}/${code}` });
  if (apiResult.data.success) {
    let data = apiResult.data;
    return { ...data.ticker, timestamp: data.timestamp };
  } else {
    // logger.error(`Currency [${code}] failed to get the realtime data, Error: ${apiResult.data.error}`);
  }
};

module.exports = queryCryptoCurrencyRealtimeData;
