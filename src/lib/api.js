let axios = require("axios");

module.exports = async options => {
  let result;
  result = await axios(options).catch(e => (result = e.response));
  return result;
};
