const { HTTP_STATUS } = require("./constants");
const moment = require("moment");
class CommonError extends Error {
  constructor({ status = HTTP_STATUS.BAD_REQUEST, code, message }) {
    super();
    this.success = false;
    this.status = status;
    this.code = code;
    this.message = message;
    this.moment = moment().format();
  }
}

module.exports = CommonError;
