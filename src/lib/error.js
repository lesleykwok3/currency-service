const moment = require("moment");
const { HTTP_STATUS, USER_ROLE } = require("./constants");
const logger = require("./logger");
const CommonError = require("./CommonError");

module.exports.assertBodyField = (
  ctx,
  fieldNames,
  code = "BR-001",
  status = HTTP_STATUS.BAD_REQUEST
) => {
  if (!ctx.request)
    throw new CommonError({
      status,
      code,
      message: `${fieldNames.join(", ")} are required`
    });
  if (!ctx.request.body)
    throw new CommonError({
      status,
      code,
      message: `${fieldNames.join(", ")} are required`
    });

  let givenFields = Object.getOwnPropertyNames(ctx.request.body);
  if (fieldNames.find(n => givenFields.indexOf(n) == -1)) {
    throw new CommonError({
      status,
      code,
      message: `${fieldNames.join(", ")} are required`
    });
  }
};

module.exports.assertAuth = ctx => {
  if (!ctx.auth) {
    throw new CommonError({
      status: HTTP_STATUS.UNAUTHORIZED,
      code: "AU-006",
      message: "Login First"
    });
  }
};

module.exports.assertPermission = (ctx, device) => {
  if (!ctx.auth) {
    throw new CommonError({
      status: HTTP_STATUS.UNAUTHORIZED,
      code: "AU-006",
      message: "Login First"
    });
  }
  let { role, community } = ctx.auth;
  if (role !== USER_ROLE.SUPPER_ADMIN && device.ownerCommunity !== community) {
    throw new CommonError({
      status: HTTP_STATUS.UNAUTHORIZED,
      code: "AU-007",
      message:
        "Permission Denies: you don't have the permission to view this device."
    });
  }
};
