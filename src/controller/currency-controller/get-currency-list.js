const moment = require("moment");
const { PassThrough } = require("stream");
const { HTTP_STATUS, CURRENCY_LIST } = require("../../lib/constants");
const logger = require("../../lib/logger");
const CommonError = require("../../lib/CommonError");
const queryCryptoRealtimeData = require("../../lib/query-crypto-realtime-data");
const eventEmitter = require("../../lib/event-emitter");

setInterval(async () => {
  // console.log(eventEmitter.listenerCount("data"));
  let listenerCount = eventEmitter.listenerCount("data");
  if (listenerCount > 0) {
    try {
      let currencyRealtimeApiResult = await Promise.all(
        CURRENCY_LIST.map(code => queryCryptoRealtimeData(code))
      );
      currencyRealtimeApiResult = currencyRealtimeApiResult.filter(
        result => result
      );
      let data = {
        data: currencyRealtimeApiResult,
        timestamp: moment.valueOf()
      };
      eventEmitter.emit("data", JSON.stringify(data));
    } catch (error) {
      logger.error(error);
      throw new CommonError({
        status: HTTP_STATUS.BAD_REQUEST,
        message: `Fetch currency realtime data from crypto api failed, Error: ${error.message}`
      });
    }
  }
}, 1000);

module.exports = async ctx => {
  ctx.request.socket.setTimeout(0);
  ctx.req.socket.setNoDelay(true);
  ctx.req.socket.setKeepAlive(true);
  ctx.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    Connection: "keep-alive"
  });

  const stream = new PassThrough();
  ctx.status = HTTP_STATUS.OK;
  ctx.body = stream;

  const listener = data => {
    stream.write(`data: ${data}\n\n`);
  };

  eventEmitter.on("data", listener);

  stream.on("close", () => {
    eventEmitter.off("data", listener);
  });
};
